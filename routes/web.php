<?php

use App\Http\Controllers\API\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::group(['middleware' => ['cors', 'json.response']], function () {

//     // ...


//     Route::post('/login', [LoginController::class,'login'])->name('login.api');
//     Route::post('/register',[LoginController::class,'register'])->name('register.api');
//     Route::post('/logout', [LoginController::class,'logout'])->name('logout.api');

//     // ...

// });

Route::get('/log', [LoginController::class,'userLoginIndex']);
Route::get('/reg',[LoginController::class,'index']);
