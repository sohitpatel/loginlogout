<?php

use App\Http\Controllers\API\LoginController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// // Route::middleware('auth:api')->get('/user', function (Request $request) {
// //     return $request->user();
// // });
// Route::post('login', [LoginController::class,'login']);
// Route::post('register', [LoginController::class,'register']);
// //Route::group(['middleware' => 'auth:api'], function(){
// Route::post('details', [LoginController::class,'details']);
// //});

// Route::group(['middleware' => ['cors', 'json.response']], function () {

//     // ...`

//     // public routes
//     Route::post('/login', [LoginController::class,'login'])->name('login.api');
//     Route::post('/register',[LoginController::class,'register'])->name('register.api');
//     Route::post('/logout', [LoginController::class,'logout'])->name('logout.api');

//     // ...

// });
